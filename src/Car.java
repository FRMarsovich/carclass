class Transport {
    public Engine engine = new Engine();
    public void carry() {}
}

class Engine {
    public void start() {}
    public void stop(){}
}

class Whell {
    public void roll() {}
}

class Window {
    public void toCatchFlies() {}
    public void toBeTransparent() {}
}

class GasTank {
    public void fuelStorage() {}
}

public class Car extends Transport {
    public GasTank gasTank = new GasTank();
    public Window frontWindow = new Window();
    public Window backWindow = new Window();
    public Whell[] whells = new Whell[4];
    public Car() {
        for (int i = 0; i<4; i++) {
            whells[i] = new Whell();
        }
    }
    public static void main(String[] args) {
        Car car = new Car();
        if (car instanceof Transport) {
            System.out.println("Автомобиль - это транспорт с бензобаком, 2 окнами, 4 колесами");
        } else {
            //это условие не выполняется, поэтому даже прописывать его не будем
        }
        if (car instanceof Object) {
            System.out.println("Автомобиль - это объект");
        }
    }
}


